<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cache', function () {

    Artisan::call("config:cache");
    Artisan::call("cache:clear");
    echo "clear";
});

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin', function () {
    return view('admin.login');
});
//new routes
Route::get('/dashboard', function () {
    return view('admin.index');
});

Route::post('/login','Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

Route::resource('/projects','ProjectsController');
Route::get('/project-delete/{id}','ProjectsController@destroy');
Route::get('/project-photos/{id}','ProjectsController@photos');
Route::post('/project-photos-upload/{id}','ProjectsController@uploadPhotos');
Route::get('/project-photos-delete/{id}','ProjectsController@deletePhotos');

Route::get('/project-location-map/{id}','ProjectsController@locationMap');
Route::post('/project-location-map-upload/{id}','ProjectsController@locationMapUpload');
Route::get('/project-location-map-delete/{id}','ProjectsController@locationMapDelete');

Route::get('/project-layout-plan/{id}','ProjectsController@layoutPlan');
Route::post('/project-layout-plan-upload/{id}','ProjectsController@layoutPlanUpload');
Route::get('/project-layout-plan-delete/{id}','ProjectsController@layoutPlanDelete');


Route::resource('purchase','PurchaseController');
Route::get('purchase-payments/{id}','PurchaseController@purchasePayments');
Route::post('purchase-payments/{id}','PurchaseController@storePurchasePayments');
Route::get('list-purchase-payments/{id}','PurchaseController@purchasePaymentList');
Route::get('edit-purchase-payments/{id}','PurchaseController@purchasePaymentEdit');
Route::post('update-purchase-payments/{id}','PurchaseController@purchasePaymentUpdate');

Route::resource('properties','PropertiesController');
Route::get('properties-delete/{id}','PropertiesController@destroy');
