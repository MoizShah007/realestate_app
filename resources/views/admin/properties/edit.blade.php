@extends('admin.layout.app')
@section('content')
    <div class="content-i">
        <div class="content-box">
            <div class="row">
                <div class="col-sm-12">
                    <div class="element-wrapper">
                        <h6 class="element-header">
                            Create Properties
                        </h6>
                        @include('shared.errors')

                        <div class="element-box">
                            <form action="{{ url('properties/'.$properties->id) }}" id="formValidate" method="post"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <input name="_method" type="hidden" value="PUT">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Projects</label>
                                            <select class="form-control" required="required" name="project_id">
                                                <option value="">
                                                    Select Project
                                                </option>
                                                @foreach($projects as $value)
                                                    <option value="{{ $value->id }}">
                                                        {{ $value->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Property Name</label>
                                            <input class="form-control" required="required" type="text"
                                                   name="name" value="{{ $properties->name }}">
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Type</label>
                                            <input class="form-control" required="required" type="radio"
                                                   name="type" value="Commercial">Commercial
                                            <input class="form-control" required="required" type="radio"
                                                   name="type" value="Residential">Residential
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Availiable For</label>
                                            <input class="form-control" required="required" type="radio"
                                                   name="availiable" value="sale">Sale
                                            <input class="form-control" required="required" type="radio"
                                                   name="availiable" valur="rent">Rent
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Comment/Remarks</label>
                                            <textarea class="form-control" required="required"
                                                      name="remarks">{{ $properties->remarks }}</textarea>
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Add Photo</label>
                                            <input class="form-control" type="file" name="photo">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Add Document</label>
                                            <input class="form-control"  type="file" name="photo">
                                        </div>
                                    </div>

                                </div>
                                <div class="form-buttons-w">
                                    <button class="btn btn-primary" type="submit">Update</button>
                                    <a href="{{ url('properties') }}" class="btn btn-danger">Close</a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
@stop