@extends('admin.layout.app')
@section('content')
    <div class="content-i">
        <div class="content-box">
            <div class="element-wrapper">
                <h6 class="element-header">
                    Properties
                </h6>
                <div class="element-box">
                    <h5 class="form-header">
                        <a href="{{ url('projects/create') }}" class="mr-2 mb-2 btn btn-success" > Add Properties</a>
                    </h5>

                    <div class="table-responsive">
                        <table id="dataTable1" width="100%" class="table table-striped table-lightfont"><thead><tr><th>Project Name</th><th>City</th><th>State</th><th>Action</th></tr></thead>


                            <tbody>
                            @foreach($properties as $value)
                                <tr>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->type }}</td>
                                    <td>{{ $value->availiable }}</td>
                                    <td><div class="btn-group mr-1 mb-1">
                                            <button aria-expanded="false" aria-haspopup="true" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="dropdownMenuButton1" type="button">Action</button>
                                            <div aria-labelledby="dropdownMenuButton1" class="dropdown-menu">
                                                <a class="dropdown-item" href="{{ url('project-photos/'.$value->id) }}"> Photos</a>
                                                <a class="dropdown-item" href="{{ url('project-layout-plan/'.$value->id) }}"> Layout Plan</a>
                                                <a class="dropdown-item" href="{{ url('project-location-map/'.$value->id) }}"> Flats/Plot</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="{{ url('properties/'.$value->id) }}"> View</a>
                                                <a class="dropdown-item" href="{{ url('properties-delete/'.$value->id) }}"> Delete</a>
                                                <a class="dropdown-item" href="{{ url('properties/'.$value->id.'/edit') }}"> Update</a>
                                            </div>
                                        </div></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop