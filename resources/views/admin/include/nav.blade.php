<div class="logged-user-w avatar-inline">
    <div class="logged-user-i">
        <div class="avatar-w">
            <img alt="" src="{{ url('img/avatar1.jpg') }}">
        </div>
        <div class="logged-user-info-w">
            <div class="logged-user-name">
                Admin
            </div>
            <div class="logged-user-role">
                Administrator
            </div>
        </div>
        <div class="logged-user-toggler-arrow">
            <div class="os-icon os-icon-chevron-down"></div>
        </div>
        <div class="logged-user-menu color-style-bright">
            <div class="logged-user-avatar-info">
                <div class="avatar-w">
                    <img alt="" src="{{ url('img/avatar1.jpg') }}">
                </div>
                <div class="logged-user-info-w">
                    <div class="logged-user-name">
                        Admin
                    </div>
                    <div class="logged-user-role">
                        Administrator
                    </div>
                </div>
            </div>
            <div class="bg-icon">
                <i class="os-icon os-icon-wallet-loaded"></i>
            </div>
            <ul>
                <li>
                    <a href="apps_email.html"><i
                                class="os-icon os-icon-mail-01"></i><span>Incoming Mail</span></a>
                </li>
                <li>
                    <a href="users_profile_big.html"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
                </li>
                <li>
                    <a href="users_profile_small.html"><i class="os-icon os-icon-coins-4"></i><span>Billing Details</span></a>
                </li>
                <li>
                    <a href="#"><i class="os-icon os-icon-others-43"></i><span>Notifications</span></a>
                </li>
                <li>
                    <a href="{{ url('/logout') }}"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="menu-actions">
    <!--------------------
    START - Messages Link in secondary top menu
    -------------------->
    <div class="messages-notifications os-dropdown-trigger os-dropdown-position-right">
        <i class="os-icon os-icon-mail-14"></i>
        <div class="new-messages-count">
            12
        </div>
        <div class="os-dropdown light message-list">
            <ul>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="img/avatar1.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">
                                John Mayers
                            </h6>
                            <h6 class="message-title">
                                Account Update
                            </h6>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="img/avatar2.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">
                                Phil Jones
                            </h6>
                            <h6 class="message-title">
                                Secutiry Updates
                            </h6>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="img/avatar3.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">
                                Bekky Simpson
                            </h6>
                            <h6 class="message-title">
                                Vacation Rentals
                            </h6>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="img/avatar4.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">
                                Alice Priskon
                            </h6>
                            <h6 class="message-title">
                                Payment Confirmation
                            </h6>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!--------------------
    END - Messages Link in secondary top menu
    --------------------><!--------------------
            START - Settings Link in secondary top menu
            -------------------->
    <div class="top-icon top-settings os-dropdown-trigger os-dropdown-position-right">
        <i class="os-icon os-icon-ui-46"></i>
        <div class="os-dropdown">
            <div class="icon-w">
                <i class="os-icon os-icon-ui-46"></i>
            </div>
            <ul>
                <li>
                    <a href="users_profile_small.html"><i class="os-icon os-icon-ui-49"></i><span>Profile Settings</span></a>
                </li>
                <li>
                    <a href="users_profile_small.html"><i class="os-icon os-icon-grid-10"></i><span>Billing Info</span></a>
                </li>
                <li>
                    <a href="users_profile_small.html"><i class="os-icon os-icon-ui-44"></i><span>My Invoices</span></a>
                </li>
                <li>
                    <a href="users_profile_small.html"><i class="os-icon os-icon-ui-15"></i><span>Cancel Account</span></a>
                </li>
            </ul>
        </div>
    </div>
    <!--------------------
    END - Settings Link in secondary top menu
    --------------------><!--------------------
            START - Messages Link in secondary top menu
            -------------------->
    <div class="messages-notifications os-dropdown-trigger os-dropdown-position-right">
        <i class="os-icon os-icon-zap"></i>
        <div class="new-messages-count">
            4
        </div>
        <div class="os-dropdown light message-list">
            <div class="icon-w">
                <i class="os-icon os-icon-zap"></i>
            </div>
            <ul>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="img/avatar1.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">
                                John Mayers
                            </h6>
                            <h6 class="message-title">
                                Account Update
                            </h6>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="img/avatar2.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">
                                Phil Jones
                            </h6>
                            <h6 class="message-title">
                                Secutiry Updates
                            </h6>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="img/avatar3.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">
                                Bekky Simpson
                            </h6>
                            <h6 class="message-title">
                                Vacation Rentals
                            </h6>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="img/avatar4.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">
                                Alice Priskon
                            </h6>
                            <h6 class="message-title">
                                Payment Confirmation
                            </h6>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!--------------------
    END - Messages Link in secondary top menu
    -------------------->
</div>
<ul class="main-menu">
    <li class="sub-header">
        <span>Layouts</span>
    </li>
    <li class="selected">
        <a href="{{ url('/dashboard') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Dashboard</span></a>
    </li>
    <li class="selected">        <a href="{{ url('/projects') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Projects</span></a>
    </li>
    <li class="selected">        <a href="{{ url('/dashboard') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Purchase</span></a>
    </li>
    <li class="selected">        <a href="{{ url('/dashboard') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Properties</span></a>
    </li>
    <li class="selected">        <a href="{{ url('/dashboard') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Plans</span></a>
    </li>
    <li class="selected">        <a href="{{ url('/dashboard') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Leads</span></a>
    </li>
    <li class="selected">        <a href="{{ url('/dashboard') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Clients</span></a>
    </li>
    <li class="selected">        <a href="{{ url('/dashboard') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Deals</span></a>
    </li>

    <li class=" has-sub-menu">
        <a href="layouts_menu_top_image.html">
            <div class="icon-w">
                <div class="os-icon os-icon-layers"></div>
            </div>
            <span>Invoices</span></a>
        <div class="sub-menu-w">
            <div class="sub-menu-header">
                Invoices
            </div>
            <div class="sub-menu-icon">
                <i class="os-icon os-icon-layers"></i>
            </div>
            <div class="sub-menu-i">
                <ul class="sub-menu">
                    <li>
                        <a href="layouts_menu_side_full.html">Balance Invoices</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Paid Invoices</a>
                    </li>
                </ul>
            </div>
        </div>
    </li>
    <li class=" has-sub-menu">
        <a href="layouts_menu_top_image.html">
            <div class="icon-w">
                <div class="os-icon os-icon-layers"></div>
            </div>
            <span>Expense / Inventory</span></a>
        <div class="sub-menu-w">
            <div class="sub-menu-header">
                Expense / Inventory
            </div>
            <div class="sub-menu-icon">
                <i class="os-icon os-icon-layers"></i>
            </div>
            <div class="sub-menu-i">
                <ul class="sub-menu">
                    <li>
                        <a href="layouts_menu_side_full.html">Category</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Vendor / Staff</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Expenses</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Inventory</a>
                    </li>
                </ul>
            </div>
        </div>
    </li>
    <li class=" has-sub-menu">
        <a href="layouts_menu_top_image.html">
            <div class="icon-w">
                <div class="os-icon os-icon-layers"></div>
            </div>
            <span>Reports</span></a>
        <div class="sub-menu-w">
            <div class="sub-menu-header">
                Reports
            </div>
            <div class="sub-menu-icon">
                <i class="os-icon os-icon-layers"></i>
            </div>
            <div class="sub-menu-i">
                <ul class="sub-menu">
                    <li>
                        <a href="layouts_menu_side_full.html">Purchase Report</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Sales Report</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Expense Report</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Profit Report</a>
                    </li>
                </ul>
            </div>
        </div>
    </li>
    <li class="selected">
        <a href="{{ url('/dashboard') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Agents</span></a>
    </li>
    <li class=" has-sub-menu">
        <a href="layouts_menu_top_image.html">
            <div class="icon-w">
                <div class="os-icon os-icon-layers"></div>
            </div>
            <span>Email & SMS</span></a>
        <div class="sub-menu-w">
            <div class="sub-menu-header">
                Email & SMS
            </div>
            <div class="sub-menu-icon">
                <i class="os-icon os-icon-layers"></i>
            </div>
            <div class="sub-menu-i">
                <ul class="sub-menu">
                    <li>
                        <a href="layouts_menu_side_full.html">Email Settings</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Email Templates</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">SMS Emails</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">SMS Setting</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">SMS Templates</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Send SMS</a>
                    </li>
                </ul>
            </div>
        </div>
    </li>
    <li class=" has-sub-menu">
        <a href="layouts_menu_top_image.html">
            <div class="icon-w">
                <div class="os-icon os-icon-layers"></div>
            </div>
            <span>Contents</span></a>
        <div class="sub-menu-w">
            <div class="sub-menu-header">
                Contents
            </div>
            <div class="sub-menu-icon">
                <i class="os-icon os-icon-layers"></i>
            </div>
            <div class="sub-menu-i">
                <ul class="sub-menu">
                    <li>
                        <a href="layouts_menu_side_full.html">Page</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Slides</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Gallery</a>
                    </li>
                </ul>
            </div>
        </div>
    </li>
    <li class="selected">
        <a href="{{ url('/dashboard') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Users</span></a>
    </li>
    <li class=" has-sub-menu">
        <a href="layouts_menu_top_image.html">
            <div class="icon-w">
                <div class="os-icon os-icon-layers"></div>
            </div>
            <span>Configurations</span></a>
        <div class="sub-menu-w">
            <div class="sub-menu-header">
                Configurations
            </div>
            <div class="sub-menu-icon">
                <i class="os-icon os-icon-layers"></i>
            </div>
            <div class="sub-menu-i">
                <ul class="sub-menu">
                    <li>
                        <a href="layouts_menu_side_full.html">General</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Category</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Payment Type</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Currency</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Tax</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Units</a>
                    </li>
                    <li>
                        <a href="layouts_menu_side_full.html">Organisation Logo</a>
                    </li>
                </ul>
            </div>
        </div>
    </li>
    <li class="selected">
        <a href="{{ url('/dashboard') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Help</span></a>
    </li>
</ul>