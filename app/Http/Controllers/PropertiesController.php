<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projects;
use App\Properties;
use Session;
class PropertiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = Properties::get();
        return view('admin.properties.index',compact('properties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Projects::get();
        return view('admin.properties.create',compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'project_id' => 'required',
            'name' => 'required',
            'type' => 'required',
            'availiable' => 'required',
            'remarks' => 'required',
        ]);
        $data = $request->all();
        Properties::create($data);
        $msg = "Properties  is created";
        Session::flash('success', $msg);
        return redirect('/properties');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $properties = Properties::where('id',$id)->first();
        return view('admin.properties.view',compact('properties'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects = Projects::get();
        $properties = Properties::where('id',$id)->first();
        return view('admin.properties.edit',compact('properties','projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'project_id' => 'required',
            'name' => 'required',
            'type' => 'required',
            'availiable' => 'required',
            'remarks' => 'required',
        ]);
        $data = $request->all();
        $data = request()->except(['_token', '_method']);
        Properties::where('id',$id)->update($data);
        $msg = "Properties  is created";
        Session::flash('success', $msg);
        return redirect('/properties');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $properties = Properties::where('id',$id)->first();
        $properties->delete();
        $msg = "Property deleted";
        Session::flash('success', $msg);
        return redirect('/properties');

    }
}
