<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Properties extends Model
{
    protected $fillable = [
        'project_id','name','type','availiable','remarks','status'
    ];

    public $timestamps = false;
}
