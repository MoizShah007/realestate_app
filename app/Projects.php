<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    protected $table = 'projects';

    protected $fillable = [
        'category_id','name','city','state','address','nearest_location','reach','purchase','description'
    ];

    public $timestamps = false;

}
